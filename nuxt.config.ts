// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['nuxt-content-assets', '@nuxt/content', '@nuxt/ui'],
  app: {
    baseURL: process.env.NODE_ENV === 'production' ? '/ostrov' : '/'
  }
})