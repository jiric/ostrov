I oblečení nám může pomoci dosáhnout našeho potenciálu. Z nebiologického oblečení se uvolňují endokriní disruptory, které mohou nabourat náš hormonální balanc.

Není však třeba draze měnit celý šatník, já volím hybridní cestu výměny spodního prádla a pyžama za zdravé bio bavlněné prádlo.

### Trenýrky
- https://www.ceskabiomoda.cz/biobavlnene-trenyrky-pro-pohodli/

### Ponožky
- https://www.ceskabiomoda.cz/bio-ponozky/

### Pyžamo
- https://www.ceskabiomoda.cz/panske-pyzamo-dlouhe/

Krom toho volím barefoot boty a to z několika důvodů. Kvůli ochraně nohou je v moderním světě lepší chodit v botách. Moderní boty jsou ovšem zdrojem mnoha problémů. Přestaly totiž sloužit funkci ochrany nohy před vnějšími vlivy a staly se především módním doplňkem. To sebou nese několik problémů.

Zúžená špička boty tlačí palce u nohou na sebe a vytváří problém vbočeného palce.

![Barefoot](barefoot.png)

Vyvýšená pata navíc kazí postoj. Je tedy vhodné boty nahradit barefoot obuví, která ovlivňuje postoj člověka minimálně a je spíše pouze pokrývkou a ochranou noh.

![Drop](drop.png)

### Barefootky
- společenské
	- https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-geo-court-iii-mens-obsidian
	- https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-gobi-boot-mens-obsidian
- horské
	- https://www.vivobarefoot.cz/produkty/panske/panske-outdoorove/vivobarefoot-tracker-decon-low-fg2-mens-sage
	- https://www.vivobarefoot.cz/produkty/panske/panske-outdoorove/vivobarefoot-tracker-decon-fg2-jjf-mens-dark-olive
- zimní
	- https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-gobi-boot-winterised-mens-tan
	- https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-gobi-iii-winter-mens-black
- sportovní
	- [https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-primus-lite-iii-mens-obsidian#bmSizes](https://www.vivobarefoot.cz/produkty/panske/vivobarefoot-primus-lite-iii-mens-obsidian#bmSizes)